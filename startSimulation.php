<?php
require_once(__DIR__.'/app/PassengerElevatorSim.php');

$passengerCount = 1;
$floorCount = 5;
if(1 == count($argv)){
    echo "===============================" . PHP_EOL;
    echo "startSimulation.php usage:" . PHP_EOL;
    echo "php startSimulation.php -p[passengerCount] -f[floorCount] " . PHP_EOL;
    echo "Using default values: passengerCount = 1, floorCount = 5" . PHP_EOL;
    echo "===============================" . PHP_EOL;
} else {
    $options = getopt('p::f::');
    if(is_int((int)$options['p']) && 0 < (int)$options['p']){
        $passengerCount = $options['p'];
    }
    if(is_int((int)$options['f']) && 0 < (int)$options['f']){
        $floorCount = $options['f'];
    }
    echo "===============================" . PHP_EOL;
    echo "startSimulation.php usage:" . PHP_EOL;
    echo "php startSimulation.php [passengerCount] [floorCount] " . PHP_EOL;
    echo "Using values: passengerCount = $passengerCount, floorCount = $floorCount" . PHP_EOL;
    echo "===============================" . PHP_EOL;
}

(new PassengerElevatorSim($passengerCount, $floorCount))->startSimulation();