<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'ElevatorVendor\\PassengerElevator\\Cars\\Car' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Cars/Car.php',
    'ElevatorVendor\\PassengerElevator\\Cars\\CarButton' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Cars/CarButton.php',
    'ElevatorVendor\\PassengerElevator\\Cars\\CarButtonControl' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Cars/CarButtonControl.php',
    'ElevatorVendor\\PassengerElevator\\Dispatchers\\Dispatcher' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Dispatchers/Dispatcher.php',
    'ElevatorVendor\\PassengerElevator\\Doors\\Door' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Doors/Door.php',
    'ElevatorVendor\\PassengerElevator\\Doors\\DoorControl' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Doors/DoorControl.php',
    'ElevatorVendor\\PassengerElevator\\Doors\\DoorMotor' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Doors/DoorMotor.php',
    'ElevatorVendor\\PassengerElevator\\Drives\\Drive' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Drives/Drive.php',
    'ElevatorVendor\\PassengerElevator\\Drives\\DriveControl' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Drives/DriveControl.php',
    'ElevatorVendor\\PassengerElevator\\ElevatorControls\\ElevatorControl' => $baseDir . '/app/ElevatorVendor/PassengerElevator/ElevatorControls/ElevatorControl.php',
    'ElevatorVendor\\PassengerElevator\\Elevators\\Elevator' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Elevators/Elevator.php',
    'ElevatorVendor\\PassengerElevator\\Floors\\Floor' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Floors/Floor.php',
    'ElevatorVendor\\PassengerElevator\\Floors\\FloorButton' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Floors/FloorButton.php',
    'ElevatorVendor\\PassengerElevator\\Floors\\FloorButtonControl' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Floors/FloorButtonControl.php',
    'ElevatorVendor\\PassengerElevator\\Passengers\\Passenger' => $baseDir . '/app/ElevatorVendor/PassengerElevator/Passengers/Passenger.php',
);
