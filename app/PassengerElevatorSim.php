<?php
require_once(__DIR__ . '/../vendor/autoload.php');

use ElevatorVendor\PassengerElevator\Passengers\Passenger as Passenger;
use ElevatorVendor\PassengerElevator\Elevators\Elevator as Elevator;
use ElevatorVendor\PassengerElevator\Cars\Car as Car;
use ElevatorVendor\PassengerElevator\Cars\CarButton as CarButton;
use ElevatorVendor\PassengerElevator\Cars\CarButtonControl as CarButtonControl;
use ElevatorVendor\PassengerElevator\Floors\Floor as Floor;
use ElevatorVendor\PassengerElevator\Floors\FloorButton as FloorButton;
use ElevatorVendor\PassengerElevator\Floors\FloorButtonControl as FloorButtonControl;

/**
 * Class PassengerElevatorSim
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class PassengerElevatorSim
{
    /**
     * @var int $passengerCount Passenger count.
     */
    private $passengerCount = 0;
    /**
     * @var Passenger[] $passengers Array of Passenger objects.
     */
    private $passengers = [];
    /**
     * @var Elevator $passengerElevator PassengerElevator object.
     */
    private $passengerElevator = null;
    /**
     * @var int $floorCount Floor count.
     */
    private $floorCount = 0;

    /**
     * PassengerElevatorSim constructor.
     * @param int $passengerCount Passenger count.
     * @param int $floorCount Floor count.
     */
    function __construct($passengerCount, $floorCount)
    {
        $this->passengerCount = $passengerCount;
        $this->floorCount = $floorCount;

        $this->generatePassengers();
        $this->generatePassengerElevator();
    }

    /**
     * Creates array of Passenger objects.
     * @return void
     */
    private function generatePassengers() {
        for($i = 0; $i < $this->passengerCount; $i++){
            $this->passengers[] = new Passenger();
        }
    }

    /**
     * Creates PassengerElevator.
     * @return void
     */
    private function generatePassengerElevator() {

        $floors = [];
        $carButtons = [];
        for($i = 0; $i < $this->floorCount; $i++){
            $floors[] = new Floor(new FloorButton($i, new FloorButtonControl()));
            $carButtons[] = new CarButton($i, new CarButtonControl());
        }
        $passengerElevator = new Elevator(new Car($carButtons), $floors);

        $this->passengerElevator = $passengerElevator;
    }

    /**
     * Starts simulation.
     *
     * Each created passenger starts by pressing a button on a random floor. After entering elevator car, passenger
     * proceeds to press a random floor button.
     *
     * @return void
     */
    function startSimulation() {
        $pe = $this->passengerElevator;

        foreach($this->passengers as $passenger) {
            $floorNumber = mt_rand(0, $this->floorCount - 1);
            echo 'Passenger from floor #' . $floorNumber . ' pressed floor button.'
                . PHP_EOL;
            $passenger->pressFloorButton($pe, $floorNumber);

            $floorNumber = mt_rand(0, $this->floorCount - 1);
            $passenger->pressCarButton($pe, $floorNumber);
        }
    }
}