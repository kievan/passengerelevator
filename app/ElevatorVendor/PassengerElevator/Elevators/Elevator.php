<?php
namespace ElevatorVendor\PassengerElevator\Elevators;

use ElevatorVendor\PassengerElevator\Cars\Car as Car;
use ElevatorVendor\PassengerElevator\Floors\Floor as Floor;

/**
 * Class PassengerElevator
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class Elevator
{
    /**
     * @var Car $car Car object.
     */
    private $car;
    /**
     * @var array $floors Array of Floor objects.
     */
    private $floors = [];
    /**
     * @var int $floorCount Floor count.
     */
    private $floorCount;

    /**
     * PassengerElevator constructor.
     * @param Car $car
     * @param array $floors
     */
    function __construct(Car $car, array $floors){
        $this->car = $car;
        $this->floors = $floors;
        $this->floorCount = count($floors);
    }

    /**
     * Returns Car object.
     * @return Car The Car object.
     */
    function getCar(){
        return $this->car;
    }
    /**
     * Returns floor count.
     * @return int Floor count.
     */
    function getFloorCount(){
        return $this->floorCount;
    }
    /**
     * Return array of Floor objects.
     * @return array Floor objects.
     */
    function getFloors(){
        return $this->floors;
    }
    /**
     * Returns Floor object specified by floor number.
     * @param int $floor
     * @return Floor
     */
    function getFloor($floor){
        return $this->floors[$floor];
    }
}