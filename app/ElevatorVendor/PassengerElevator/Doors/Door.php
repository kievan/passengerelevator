<?php
namespace ElevatorVendor\PassengerElevator\Doors;

/**
 * Class Door
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class Door
{
    /**
     * @var DoorMotor $doorMotor DoorMotor object.
     */
    private $doorMotor;

    /**
     * Door constructor.
     * @param DoorMotor $doorMotor
     */
    public function __construct($doorMotor){
        $this->doorMotor = $doorMotor;
    }

    /**
     * Opens the door.
     * @return void
     */
    function open(){
        echo "Opening door...".PHP_EOL;
        $this->doorMotor->spinOpen();
    }

    /**
     * Closes the door.
     * @return void
     */
    function close(){
        echo "Closing door...".PHP_EOL;
        $this->doorMotor->spinClose();
    }
}