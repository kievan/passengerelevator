<?php
namespace ElevatorVendor\PassengerElevator\Floors;

use ElevatorVendor\PassengerElevator\ElevatorControls\ElevatorControl as ElevatorControl;
use ElevatorVendor\PassengerElevator\Dispatchers\Dispatcher as Dispatcher;

/**
 * Class FloorButtonControl
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class FloorButtonControl implements ElevatorControl {
    /**
     * @var int $floorNumber Floor number.
     */
    private $floorNumber;

    /**
     * FloorButtonControl constructor.
     */
    function __construct()
    {
    }

    /**
     * Updates Dispatcher with a floor number.
     */
    function update(){
        echo __CLASS__ . ": sending dispatcher floor #: " . $this->floorNumber . PHP_EOL;
        Dispatcher::getInstance()->setDestinationDriveFloor($this->floorNumber);
    }

    /**
     * Sends the call from Floor the car to Dispatcher.
     *
     * @param int $floor Floor number.
     * @return void
     */
    function call($floor){
        $this->floorNumber = $floor;
        $this->update();
        Dispatcher::desiredFloor($floor);
    }

}