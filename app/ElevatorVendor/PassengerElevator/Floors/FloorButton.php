<?php
namespace ElevatorVendor\PassengerElevator\Floors;

/**
 * Class FloorButton
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class FloorButton {
    /**
     * @var int $buttonFloorNumber Button floor number.
     */
    private $buttonFloorNumber;
    /**
     * @var FloorButtonControl FloorButtonControl object.
     */
    private $floorButtonControl;

    /**
     * FloorButton constructor.
     * @param $floor
     * @param $floorButtonControl
     */
    public function __construct($floor, $floorButtonControl){
        $this->buttonFloorNumber = $floor;
        $this->floorButtonControl = $floorButtonControl;
    }

    /**
     * Return button floor number.
     * @return int Button floor number.
     */
    function getFloorNumber(){
        return $this->buttonFloorNumber;
    }

    /**
     * Initiates a car call from a floor.
     */
    function floorCall(){
        echo 'FLOOR call from floor #' . $this->buttonFloorNumber . PHP_EOL;
        $this->floorButtonControl->call($this->buttonFloorNumber);
    }
}