<?php
namespace ElevatorVendor\PassengerElevator\Passengers;

use ElevatorVendor\PassengerElevator\Elevators\Elevator as Elevator;

/**
 * Class Passenger
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class Passenger
{
    /**
     * Passenger constructor.
     */
    function __construct(){
    }

    /**
     * Submits a floor call via floor button press.
     * @param Elevator $passengerElevator
     * @param int $floor
     * @return void
     */
    function pressFloorButton(Elevator $passengerElevator, $floor){
        $passengerElevator->getFloor($floor)->getButton()->floorCall();
    }
    /**
     * Submits a car call via car button press.
     * @param Elevator $elevator
     * @param int $floor
     * @return void
     */
    function pressCarButton($elevator, $floor){
        $elevator->getCar()->getButton($floor)->carCall();
    }
}