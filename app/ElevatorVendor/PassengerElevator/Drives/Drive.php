<?php
namespace ElevatorVendor\PassengerElevator\Drives;

use ElevatorVendor\PassengerElevator\Doors\Door as Door;
use ElevatorVendor\PassengerElevator\Doors\DoorMotor as DoorMotor;
use ElevatorVendor\PassengerElevator\Doors\DoorControl as DoorControl;
use ElevatorVendor\PassengerElevator\Dispatchers\Dispatcher as Dispatcher;

/**
 * Class Drive
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class Drive
{
    /**
     * @var DriveControl $driveControl DriveControl object.
     */
    private $driveControl;
    /**
     * @var DoorControl $doorControl DoorControl object.
     */
    private $doorControl;
    /**
     * @var int $floorNumber Floor number.
     */
    private $floorNumber;
    /**
     * @var int $destinationDirection Destination direction.
     */
    private $destinationDirection;
    /**
     * @var int $destinationFloor Destination floor number.
     */
    private $destinationFloor;
    /**
     * @var int $state Drive state.
     */
    private $state;

    /**
     * Drive constructor.
     * @param DriveControl $driveControl DriveControl object.
     * @param DoorControl $doorControl DoorControl object.
     */
    function __construct(DriveControl $driveControl, DoorControl $doorControl){
        $this->driveControl = $driveControl;
        $this->doorControl = $doorControl;
    }
    /**
     * Returns DriveControl object.
     * @return DriveControl DriveControl object.
     */
    function getDriveControl(){
        return $this->driveControl;
    }
    /**
     * Calculates a drive destination direction.
     * @return int
     */
    private function calculateDestinationDriveDirection(){
        $destinationDriveDirection = 0;

        if($this->destinationFloor > $this->floorNumber){
            $destinationDriveDirection = Dispatcher::DRIVE_DEST_DIRECTION_UP;
        } else if ($this->destinationFloor === $this->floorNumber){
            $destinationDriveDirection = Dispatcher::DRIVE_DEST_DIRECTION_NONE;
        } else if ($this->destinationFloor < $this->floorNumber){
            $destinationDriveDirection = Dispatcher::DRIVE_DEST_DIRECTION_DOWN;
        }

        return $destinationDriveDirection;
    }
    /**
     * Signal that Drive has reached a destination floor. Signal to open a door.
     * @return void
     */
    private function atFloor(){
        echo 'At destination floor: '.$this->floorNumber.PHP_EOL;
        $this->driveControl->stop($this, $this->floorNumber);
        $door = new Door(new DoorMotor());
        $this->doorControl->open($door);
    }
    /**
     * Moves a Drive from a current floor to a destination floor.
     * Triggers atFloor sensor when Drive has reached destination.
     * @param int $floor Floor number.
     * @return void
     */
    function move($floor){
        $this->floorNumber = Dispatcher::getInstance()->getCurrentDriveFloor();
        $this->destinationFloor = $floor;
        $this->destinationDirection = $this->calculateDestinationDriveDirection();
        $this->state = Dispatcher::DRIVE_STATE_BUSY;

        echo 'Current floor: ' . $this->floorNumber . PHP_EOL;
        while($this->floorNumber !== $this->destinationFloor){
            sleep(Dispatcher::DRIVE_SPEED);
            if(Dispatcher::DRIVE_DEST_DIRECTION_UP === $this->destinationDirection) {
                echo 'Floor #'.$this->floorNumber.'. Moving up.'.PHP_EOL;
                $this->floorNumber++;
            } else if (Dispatcher::DRIVE_DEST_DIRECTION_DOWN === $this->destinationDirection){
                echo 'Floor #'.$this->floorNumber.'. Moving down.'.PHP_EOL;
                $this->floorNumber--;
            }
        }
        $this->atFloor();
    }

    /**
     * Stops a Drive.
     * @return void
     */
    function stop(){
        echo 'Stop car at floor #'.$this->floorNumber.PHP_EOL;
    }

}


