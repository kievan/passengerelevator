<?php
namespace ElevatorVendor\PassengerElevator\Drives;

use ElevatorVendor\PassengerElevator\Dispatchers\Dispatcher as Dispatcher;
use ElevatorVendor\PassengerElevator\ElevatorControls\ElevatorControl as ElevatorControl;

/**
 * Class DriveControl
 * @author Artyom Gordiyevsky <kievan@gmail.com>
 */
class DriveControl implements ElevatorControl {
    /**
     * @var int $floorNumber Floor number.
     */
    private $floorNumber;
    /**
     * @var int $driveState Drive state.
     */
    private $driveState;

    /**
     * DriveControl constructor.
     */
    function __construct()
    {
    }

    /**
     * Updates Dispatcher with current floor number, drive state.
     * @return void
     */
    function update(){
        $dispatcher = Dispatcher::getInstance();
        $dispatcher->setCurrentDriveFloor($this->floorNumber);
        $dispatcher->setDriveState($this->driveState);
    }
    /**
     * Moves Drive, sends Dispatcher update.
     * @param Drive $drive Drive object.
     * @param int $floor Floor number.
     * @return void
     */
    function move(Drive $drive, $floor){
        $this->driveState = Dispatcher::DRIVE_STATE_BUSY;
        $drive->move($floor);
        $this->update();
    }
    /**
     * Stops Drive, sends Dispatcher update.
     * @param Drive $drive Drive object.
     * @param int $floor Floor number.
     * @return void
     */
    function stop(Drive $drive, $floor){
        $this->floorNumber = $floor;
        $this->driveState = Dispatcher::DRIVE_STATE_IDLE;
        $drive->stop();
        $this->update();
    }
}