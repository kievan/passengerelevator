# Passenger Elevator Simulator

### Introduction
A simulation that shows how a simple elevator system works. The design is based on the paper called [A UML Documentation for an Elevator System by Lu Luo](http://www.cs.cmu.edu/~luluo/Courses/18540PhDreport.pdf "A UML Documentation for an Elevator System by Lu Luo"). Some subsystems have been omitted, along with some behaviour in the subsystems which are implemented.

### Usage

    ===============================
    startSimulation.php usage:
    php startSimulation.php -p[passengerCount] -f[floorCount]
    Using default values: passengerCount = 1, floorCount = 5
    ===============================


### Sample output:

    artem@artem3:~/PhpstormProjects/PassengerElevator$ php startSimulation.php
    ===============================
    startSimulation.php usage:
    php startSimulation.php -p[passengerCount] -f[floorCount]
    Using default values: passengerCount = 1, floorCount = 5
    ===============================
    Passenger from floor #0 pressed floor button.
    FLOOR call from floor #0
    ElevatorVendor\PassengerElevator\Floors\FloorButtonControl: sending dispatcher floor #: 0
    ElevatorVendor\PassengerElevator\Dispatchers\Dispatcher: get to floor #0
    Moving car to floor #0:
    Current floor: 0
    At destination floor: 0
    Stop car at floor #0
    Opening door...
    Spinning door motor to open door.
    Dwelling:
    .....
    Closing door...
    Spinning door motor to close door.
    CAR call to floor #2
    ElevatorVendor\PassengerElevator\Cars\CarButtonControl: sending dispatcher floor number: 2
    ElevatorVendor\PassengerElevator\Dispatchers\Dispatcher: get to floor #2
    Moving car to floor #2:
    Current floor: 0
    Floor #0. Moving up.
    Floor #1. Moving up.
    At destination floor: 2
    Stop car at floor #2
    Opening door...
    Spinning door motor to open door.
    Dwelling:
    .....
    Closing door...
    Spinning door motor to close door.
    artem@artem3:~/PhpstormProjects/PassengerElevator$

### TODOs

1. Create a simulation which will be closer to real life:
   - Change the interaction mode between Passenger and Elevator to event based (from blocking)
   - Add a messaging queue which will provide a method to record and process Passenger signals
     (now only one signal can be processed at a time)
   - Generate Passengers according to Poisson process
2. Develop abstractions further to be able to create different kinds of elevators,
   with different configurations (cargo elevator with two doors, etc.)
3. Add necessary subsystems such as Safety (emergency break), Door reversal, Car weight limit
4. Add convenience subsystems such as Elevator position indicators on the floors and in the Car
5. Implement Passenger delivery optimizations on single and multi shaft Elevator systems